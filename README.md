# `abinitio`

`abinitio` is a package for analyzing ab initio thermodynamics data.

## Installation

```bash
$ git clone https://gitlab.com/lkkmpn/abinitio.git
$ pip install ./abinitio
```

## License

`abinitio` is licensed under [the GPLv3 license.](LICENSE.md)
