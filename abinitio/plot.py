from __future__ import annotations

import functools
import itertools
import warnings
from collections import defaultdict
from dataclasses import dataclass
from numbers import Number
from typing import (TYPE_CHECKING, Any, Dict, List, Literal, Optional,
                    Sequence, Tuple, Union)

import numpy as np
from matplotlib.artist import Artist
from matplotlib.axes import Axes
from matplotlib.axes._secondary_axes import SecondaryAxis
from matplotlib.axis import Axis
from matplotlib.collections import PatchCollection
from matplotlib.figure import Figure
from matplotlib.legend import Legend
from matplotlib.lines import Line2D as MplLine2D
from matplotlib.patches import Patch, Polygon, Rectangle
from matplotlib.text import Text
from matplotlib.ticker import Formatter, Locator
from numpy.typing import NDArray
from scipy.spatial import ConvexHull

from abinitio.geometry import Line2D, Plane3D
from abinitio.species import GasPhaseSpecies, SolidPhaseSpecies
from abinitio.utils import approx_gt, approx_lt

if TYPE_CHECKING:
    from abinitio.aitd import AITD


Conditions = Dict[Literal['T', 'p'], float]


class AITDPlotter:
    def __init__(self,
                 aitd: AITD,
                 ax: Axes):
        self.aitd = aitd
        self.ax = ax

        self.x_species = None
        self.y_species = None

        self.last_axes_position = dict(bottom=0.0, left=0.0, top=None, right=None)

        self.plotted = False

        self.legend_entries: List[LegendEntry] = []

    def _plots(func):
        @functools.wraps(func)
        def wrapper(self: AITDPlotter, *args, **kwargs) -> Any:
            if self.plotted:
                raise ValueError('Can only call a plotting function once per Axes object')
            self.plotted = True
            return func(self, *args, **kwargs)
        return wrapper

    def _requires_plot(func):
        @functools.wraps(func)
        def wrapper(self: AITDPlotter, *args, **kwargs) -> Any:
            if not self.plotted:
                raise ValueError('Can only call this function after a plotting function has been called')
            return func(self, *args, **kwargs)
        return wrapper

    @_plots
    def plot_1d(self,
                x_species: GasPhaseSpecies,
                x_mu_min: float,
                x_mu_max: float,
                fixed_mus: Optional[Dict[GasPhaseSpecies, float]] = None,
                *,
                line: bool = True,
                fill: bool = True,
                fill_alpha: float = 0.6) -> None:

        if x_species not in self.aitd.gas_phase_reservoir:
            raise ValueError(f'Species {x_species} is not defined as a gas-phase species in the AITD object.')

        self.x_species = x_species

        if fixed_mus is None:
            fixed_mus = {}

        if x_species in fixed_mus:
            fixed_mus.pop(x_species)
            warnings.warn(f'Ignoring chemical potential value for {x_species}, '
                          'as this species is on the x axis.',
                          category=RuntimeWarning,
                          stacklevel=2)

        legend_species: Dict[SolidPhaseSpecies, bool] = {}  # whether the species is visible in the plot

        if line:
            for species in self.aitd.solid_phase:
                linear_equation = species.get_free_energy_equation(x_species, fixed_mus)

                dG_min = linear_equation.evaluate({x_species: x_mu_min})
                dG_max = linear_equation.evaluate({x_species: x_mu_max})

                self.ax.plot([x_mu_min, x_mu_max], [dG_min, dG_max],
                             color=species.plot_color, zorder=2.5)

                legend_species[species] = True

        if fill:
            species_lines = [species.get_free_energy_equation(x_species, fixed_mus)
                             .to_line(x_species)
                             for species in self.aitd.solid_phase
                             ]

            limits_lines = [Line2D(1.0, 0.0, -x_mu_min),
                            Line2D(1.0, 0.0, -x_mu_max)]

            # get coordinates of line intersections by the lines which include
            # these coordinates
            xs_by_line: Dict[Line2D, List[NDArray]] = defaultdict(list)

            for lines_subset in itertools.combinations(species_lines + limits_lines, 2):
                x = Line2D.two_line_intersection(*lines_subset)

                if x is None:
                    # lines are parallel
                    continue

                if approx_lt(x[0], x_mu_min) or approx_gt(x[0], x_mu_max):
                    # intersection point is outside of plotting range
                    continue

                other_lines = set(species_lines) - set(lines_subset)
                if any(l.evaluate_y(x[0]) < x[1] for l in other_lines):
                    # another species has lower ΔG at the intersection point
                    continue

                species_lines_subset = set(species_lines) & set(lines_subset)

                for line in species_lines_subset:
                    xs_by_line[line].append(x)

            for species, line in zip(self.aitd.solid_phase, species_lines):
                xs = np.array(xs_by_line[line])
                if len(xs) == 0:
                    # species is not stable at any conditions within the plot limits
                    if species not in legend_species:  # could have already been added as a line
                        legend_species[species] = False
                else:
                    self.ax.axvspan(xs[0, 0], xs[1, 0], fc=species.fill_color, alpha=fill_alpha, zorder=2)

                    legend_species[species] = True

        self.ax.set_xlim(x_mu_min, x_mu_max)

        for species in self.aitd.solid_phase:
            if (line or fill) and species in legend_species:
                patch_artist = Patch(fc=species.fill_color, alpha=fill_alpha)
                line_artist = MplLine2D([], [], c=species.plot_color, solid_capstyle='butt')

                if line and fill:
                    handle = (patch_artist, line_artist)
                elif line:
                    handle = line_artist
                elif fill:
                    handle = patch_artist

                self.legend_entries.append(LegendEntry(handle=handle,
                                                       label=species.plot_label,
                                                       visible=legend_species.get(species)))

    def get_segments_1d(self,
                        x_species: GasPhaseSpecies,
                        fixed_mus: Optional[Dict[GasPhaseSpecies, float]] = None) -> List[Segment]:

        species_by_slope: Dict[float, List[SolidPhaseSpecies]] = defaultdict(list)

        for species in self.aitd.solid_phase:
            linear_equation = species.get_free_energy_equation(x_species, fixed_mus)
            slope = linear_equation.get_coefficient(x_species)

            species_by_slope[slope].append(species)

        species_to_consider: List[SolidPhaseSpecies] = []

        for species_with_slope in species_by_slope.values():
            species_to_consider.append(
                min(species_with_slope,
                    key=lambda species: species.get_free_energy_equation(x_species, fixed_mus).get_intercept())
            )

        intersections: List[Intersection] = []

        for species1, species2 in itertools.combinations(species_to_consider, 2):
            equation1 = species1.get_free_energy_equation(x_species, fixed_mus)
            equation2 = species2.get_free_energy_equation(x_species, fixed_mus)

            slope1 = equation1.get_coefficient(x_species)
            intercept1 = equation1.get_intercept()
            slope2 = equation2.get_coefficient(x_species)
            intercept2 = equation2.get_intercept()

            x = (intercept2 - intercept1) / (slope1 - slope2)
            y = equation1.evaluate({x_species: x})

            for other in set(self.aitd.solid_phase) - {species1, species2}:
                if other.get_free_energy_equation(x_species, fixed_mus).evaluate({x_species: x}) < y:
                    break
            else:
                intersections.append(Intersection(x, species1, species2))

        intersections.sort(key=lambda intersection: intersection.x)

        segments: List[Segment] = []

        for i in range(len(intersections)):
            if i == 0:
                intersection = intersections[i]
                species = max([intersection.species1, intersection.species2],
                              key=lambda species: species.get_free_energy_equation(x_species, fixed_mus).get_coefficient(x_species))
                segments.append(Segment(-np.inf, intersection.x, species))

            if i < len(intersections) - 1:
                i1, i2 = intersections[i:i+2]
                species = list({i1.species1, i1.species2} & {i2.species1, i2.species2})[0]
                segments.append(Segment(i1.x, i2.x, species))

            if i == len(intersections) - 1:
                intersection = intersections[i]
                species = min([intersection.species1, intersection.species2],
                              key=lambda species: species.get_free_energy_equation(x_species, fixed_mus).get_coefficient(x_species))
                segments.append(Segment(intersection.x, np.inf, species))

        return segments

    @_plots
    def plot_2d(self,
                x_species: GasPhaseSpecies,
                x_mu_min: float,
                x_mu_max: float,
                y_species: GasPhaseSpecies,
                y_mu_min: float,
                y_mu_max: float,
                fixed_mus: Optional[Dict[GasPhaseSpecies, float]] = None) -> None:

        for species in [x_species, y_species]:
            if species not in self.aitd.gas_phase_reservoir:
                raise ValueError(f'Species {species} is not defined as a gas-phase species in the AITD object.')

        if x_species == y_species:
            raise ValueError('Species on the x and y axes cannot be the same.')

        self.x_species = x_species
        self.y_species = y_species

        if fixed_mus is None:
            fixed_mus = {}

        for species, axis_label in [(x_species, 'x'), (y_species, 'y')]:
            if species in fixed_mus:
                fixed_mus.pop(species)
                warnings.warn(f'Ignoring chemical potential value for {species}, '
                              f'as this species is on the {axis_label} axis.',
                              category=RuntimeWarning,
                              stacklevel=2)

        species_planes = [species
                          .get_free_energy_equation([x_species, y_species], fixed_mus)
                          .to_plane(x_species, y_species)
                          for species in self.aitd.solid_phase]

        limits_planes = [Plane3D(1.0, 0.0, 0.0, -x_mu_min),
                         Plane3D(1.0, 0.0, 0.0, -x_mu_max),
                         Plane3D(0.0, 1.0, 0.0, -y_mu_min),
                         Plane3D(0.0, 1.0, 0.0, -y_mu_max)]

        # get coordinates of plane intersections by the planes which include
        # these coordinates
        xs_by_plane: Dict[Plane3D, List[NDArray]] = defaultdict(list)

        for planes_subset in itertools.combinations(species_planes + limits_planes, 3):
            x = Plane3D.three_plane_intersection(*planes_subset)

            if x is None:
                # planes are parallel
                continue

            if approx_lt(x[0], x_mu_min) or approx_gt(x[0], x_mu_max) or \
                    approx_lt(x[1], y_mu_min) or approx_gt(x[1], y_mu_max):
                # intersection point is outside of plotting range
                continue

            other_planes = set(species_planes) - set(planes_subset)
            if any(p.evaluate_z(*x[:2]) < x[2] for p in other_planes):
                # another species has lower ΔG at the intersection point
                continue

            species_planes_subset = set(species_planes) & set(planes_subset)

            for plane in species_planes_subset:
                xs_by_plane[plane].append(x)

        patches: List[Polygon] = []

        for species, plane in zip(self.aitd.solid_phase, species_planes):
            xs = np.array(xs_by_plane[plane])

            if len(xs) == 0:
                # species is not stable at any conditions within the plot limits
                visible = False
            else:
                hull = ConvexHull(xs[:, :2])  # `hull.vertices` are in counterclockwise order for 2D points

                # draw two polygons: edge (behind) and fill (in front) to prevent artifacts between edges of polygons
                p_edge = Polygon(xs[hull.vertices, :2], fc='none', ec=species.fill_color, lw=1.0, zorder=2)
                p_fill = Polygon(xs[hull.vertices, :2], fc=species.fill_color, ec='none', zorder=2.5)

                patches += [p_edge, p_fill]

                visible = True

            self.legend_entries.append(LegendEntry(handle=Patch(fc=species.fill_color),
                                                   label=species.plot_label,
                                                   visible=visible))

        pc = PatchCollection(patches, match_original=True)
        self.ax.add_collection(pc)

        self.ax.set_xlim(x_mu_min, x_mu_max)
        self.ax.set_ylim(y_mu_min, y_mu_max)

    @_requires_plot
    def xlabel(self, **kwargs) -> Text:

        if self.x_species is None:
            raise ValueError('Cannot set x label without species on the x axis')

        text = rf'$\Delta \mu_{{{self.x_species.mu_formula.format(wrap_math=False)}}}$ (eV)'

        return self.ax.set_xlabel(text, **kwargs)

    @_requires_plot
    def ylabel(self, **kwargs) -> Text:

        if self.y_species is None:  # assume 1D plot
            text = r'$\Delta G$ (eV)'
        else:
            text = rf'$\Delta \mu_{{{self.y_species.mu_formula.format(wrap_math=False)}}}$ (eV)'

        return self.ax.set_ylabel(text, **kwargs)

    @_requires_plot
    def legend(self,
               *,
               only_visible: bool = True,
               figure: Optional[Figure] = None,
               **kwargs) -> Legend:

        handles, labels = self.get_legend_handles_labels(only_visible=only_visible)
        artist = self.ax if figure is None else figure

        return artist.legend(handles, labels, **kwargs)

    @_requires_plot
    def get_legend_handles_labels(self,
                                  *,
                                  only_visible: bool = True) -> Tuple[List[Artist], List[str]]:

        handles = [legend_entry.handle for legend_entry in self.legend_entries
                   if legend_entry.should_return(only_visible)]
        labels = [legend_entry.label for legend_entry in self.legend_entries
                  if legend_entry.should_return(only_visible)]

        return handles, labels

    @_requires_plot
    def add_axis(self,
                 species: GasPhaseSpecies,
                 *,
                 log10_pressure: Optional[float] = None,
                 temperature: Optional[float] = None,
                 show_secondary_axis: bool = True,
                 show_minor_ticks_on_main_axis: bool = True,
                 offset: float = 40.0,
                 same_side: bool = True,
                 label: bool = True,
                 axis_kwargs: Optional[Dict[str, Any]] = None,
                 label_kwargs: Optional[Dict[str, Any]] = None,
                 tick_params: Optional[Dict[str, Any]] = None,
                 locator: Optional[Locator] = None,
                 formatter: Optional[Formatter] = None) -> SecondaryAxis:

        if not ((log10_pressure is None) ^ (temperature is None)):  # xor: only one is None
            raise ValueError('Should specify one of `log10_pressure` or `temperature`')

        if species == self.x_species:
            axis_name = 'xaxis'
            main_axis = self.ax.xaxis
            location = 'bottom' if same_side else 'top'
            new_axis_function = self.ax.secondary_xaxis
            label_function_name = 'set_xlabel'
        elif species == self.y_species:
            axis_name = 'yaxis'
            main_axis = self.ax.yaxis
            location = 'left' if same_side else 'right'
            new_axis_function = self.ax.secondary_yaxis
            label_function_name = 'set_ylabel'
        else:
            raise ValueError(f'Species {species} is not on any of the axes.')

        # get conversion functions between dmu and either temperature or pressure
        calculator = species.chemical_potential_calculator

        if log10_pressure is not None:  # fixed pressure
            functions = (calculator._dmu_to_T(log10_pressure),
                         calculator._T_to_dmu(log10_pressure))
            label_text = f'$T$ (K), $p_{{{species.formula.format(wrap_math=False)}}}$ = $10^{{{log10_pressure}}}$ bar'
        elif temperature is not None:  # fixed temperature
            functions = (calculator._dmu_to_log10p(temperature),
                         calculator._log10p_to_dmu(temperature))
            label_text = f'$T$ = {temperature} K, $\log_{{10}} p_{{{species.formula.format(wrap_math=False)}}}/p^0$'

        # get position based on offset and previously-created axes
        last_axis_position = self.last_axes_position[location]
        new_axis_position = last_axis_position + offset if last_axis_position is not None else 0

        if show_secondary_axis:
            # only update position for next axes if this axis is visible
            self.last_axes_position[location] = new_axis_position

        # create new axis
        if axis_kwargs is None:
            axis_kwargs = {}
        if label_kwargs is None:
            label_kwargs = {}

        secondary_axis = new_axis_function(location=location, functions=functions, **axis_kwargs)
        secondary_axis.spines[location].set_position(('outward', new_axis_position))
        if label:
            secondary_axis.__getattribute__(label_function_name)(label_text, **label_kwargs)
        if tick_params:
            secondary_axis.tick_params(**tick_params)

        if locator or formatter:
            axis: Axis = secondary_axis.__getattribute__(axis_name)
            if locator:
                axis.set_major_locator(locator)
            if formatter:
                axis.set_major_formatter(formatter)

        if not show_secondary_axis:
            secondary_axis.set_visible(False)

        if show_minor_ticks_on_main_axis:
            main_axis_locator = main_axis.get_minor_locator()

            if not isinstance(main_axis_locator, SecondaryAxesLocator):
                main_axis_locator = SecondaryAxesLocator()
                main_axis.set_minor_locator(main_axis_locator)

            main_axis_locator.add_secondary_axis(secondary_axis)

        return secondary_axis

    @_requires_plot
    def highlight(self,
                  species: GasPhaseSpecies,
                  dmu: Union[float, Sequence[float]],
                  **plot_kwargs) -> None:

        if species != self.x_species and species != self.y_species:
            raise ValueError(f'Species {species} is not on any of the axes.')

        if isinstance(dmu, Number):
            dmu: Sequence[float] = [dmu]

        if len(dmu) == 0:
            return

        if len(dmu) >= 2:
            dmu = [min(dmu), max(dmu)]

        if species == self.x_species:
            highlight_function = self.ax.axvline
        if species == self.y_species:
            highlight_function = self.ax.axhline

        for v in dmu:
            highlight_function(v, **plot_kwargs)

    @_requires_plot
    def highlight_2d(self,
                     species1: GasPhaseSpecies,
                     dmu1: Union[float, Sequence[float]],
                     species2: GasPhaseSpecies,
                     dmu2: Union[float, Sequence[float]],
                     **rect_kwargs) -> None:

        for species in [species1, species2]:
            if species != self.x_species and species != self.y_species:
                raise ValueError(f'Species {species} is not on any of the axes.')

        if species1 == species2:
            raise ValueError('Species cannot be the same.')

        if isinstance(dmu1, Number):
            dmu1: Sequence[float] = [dmu1]
        if isinstance(dmu2, Number):
            dmu2: Sequence[float] = [dmu2]

        if len(dmu1) == 0 or len(dmu2) == 0:
            return

        if species1 == self.x_species:
            dmu_x, dmu_y = dmu1, dmu2
        else:
            dmu_x, dmu_y = dmu2, dmu1

        xy = [min(dmu_x), min(dmu_y)]
        width = max(dmu_x) - min(dmu_x)
        height = max(dmu_y) - min(dmu_y)

        rectangle = Rectangle(xy, width, height, **rect_kwargs)
        self.ax.add_patch(rectangle)


class SecondaryAxesLocator(Locator):
    def __init__(self):
        self.secondary_axes: List[SecondaryAxis] = []

    def add_secondary_axis(self, secondary_axis: SecondaryAxis) -> None:
        if self.axis.axis_name != secondary_axis._orientation:
            raise ValueError(f'Secondary axis is {secondary_axis._orientation} axis, '
                             f'while main axis is {self.axis.axis_name} axis')

        self.secondary_axes.append(secondary_axis)

    def __call__(self):
        vmin, vmax = self.axis.get_view_interval()
        return self.tick_values(vmin, vmax)

    def tick_values(self, vmin: float, vmax: float):
        get_ticks_function_name = f'get_{self.axis.axis_name}ticks'  # `axis_name` is 'x' or 'y'

        tick_locs = []

        for secondary_axis in self.secondary_axes:
            ticks = secondary_axis.__getattribute__(get_ticks_function_name)()
            ticks_transformed = secondary_axis._functions[1](np.array(ticks))
            tick_locs += list(ticks_transformed)

        tick_locs = [loc for loc in tick_locs if loc >= vmin and loc <= vmax]

        return self.raise_if_exceeds(tick_locs)


@dataclass
class LegendEntry:
    handle: Union[Artist, Tuple[Artist, ...]]
    label: str
    visible: bool

    def should_return(self, only_visible: bool) -> bool:
        return not only_visible or self.visible


@dataclass
class Intersection:
    x: float
    species1: SolidPhaseSpecies
    species2: SolidPhaseSpecies


@dataclass
class Segment:
    x1: float
    x2: float
    species: SolidPhaseSpecies
