from __future__ import annotations

import re
from collections.abc import MutableMapping
from typing import Iterator

from ase.atoms import Atoms
from ase.formula import Formula as AseFormula


class Formula(MutableMapping):
    def __init__(self, *args, **kwargs):
        self.update(*args, **kwargs)

    def __setitem__(self, key, value) -> None:
        if not isinstance(value, int):
            raise TypeError(f'Formula value has to be an int')
        if value <= 0:
            if key in self.__dict__:
                del self.__dict__[key]
        else:
            self.__dict__[key] = value

    def __getitem__(self, key) -> int:
        if key in self.__dict__:
            return self.__dict__[key]
        else:
            return 0

    def __delitem__(self, key) -> None:
        del self.__dict__[key]

    def __iter__(self) -> Iterator[str]:
        return iter(self.__dict__)

    def __len__(self) -> int:
        return len(self.__dict__)

    def __eq__(self, other: Formula) -> bool:
        return self.__dict__ == other.__dict__

    def count(self) -> int:
        return sum(self.__dict__.values())

    def copy(self) -> Formula:
        return Formula(self.__dict__)

    @classmethod
    def from_atoms(cls: Formula, atoms: Atoms) -> Formula:
        return cls.from_ase(atoms.symbols.formula)

    @classmethod
    def from_ase(cls: Formula, ase_formula: AseFormula) -> Formula:
        return cls(**ase_formula.count())

    @classmethod
    def from_str(cls: Formula, string_formula: str) -> Formula:
        try:
            return cls.from_ase(AseFormula(string_formula))
        except ValueError as e:
            raise ValueError(f'Could not convert {string_formula} to formula') from e

    def to_ase(self) -> AseFormula:
        return AseFormula.from_dict(self.__dict__)

    def format(self, *, wrap_math: bool = True) -> str:
        string = re.sub(r'(\d+)', r'_{\1}', str(self))
        string = fr'\mathrm{{{string}}}'
        if wrap_math:
            string = f'${string}$'
        return string

    def plot_label(self) -> str:
        return self.to_ase().convert('metal').format('latex')

    def __repr__(self) -> str:
        return f'Formula({self.__dict__})'

    def __str__(self) -> str:
        return self.to_ase().format()
