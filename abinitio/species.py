from __future__ import annotations

import functools
from typing import Any, Callable, Dict, List, Literal, Optional, Union

import numpy as np
from ase.atoms import Atoms
from ase.formula import Formula as AseFormula

from abinitio.balance import balance_equation
from abinitio.chemical_potential import IdealGasChemicalPotential
from abinitio.formula import Formula
from abinitio.linear_equation import LinearEquation


class Species:
    def __init__(self,
                 atoms: Atoms,
                 energy: Optional[float] = None):
        self._atoms = atoms

        if energy is not None:
            self._uncorrected_energy = energy
        else:
            self._uncorrected_energy = atoms.get_potential_energy()

        self.formula: Formula = Formula.from_atoms(atoms)

    @property
    def energy(self) -> float:
        return self._uncorrected_energy

    def __repr__(self):
        return f'{self.__class__.__name__}(formula={self.formula})'


class SolidPhaseSpecies(Species):
    def __init__(self,
                 atoms: Atoms,
                 energy: Optional[float] = None,
                 plot_label: Optional[Union[str, AseFormula, Formula]] = None,
                 plot_color: Optional[Any] = None,
                 fill_color: Optional[Any] = None):
        super().__init__(atoms=atoms, energy=energy)

        if plot_label is not None:
            self._plot_label = plot_label
        else:
            self._plot_label = atoms.symbols.formula

        if isinstance(self._plot_label, AseFormula):
            self._plot_label: Formula = Formula.from_ase(self._plot_label)

        self.plot_color = plot_color
        self.fill_color = fill_color

        if plot_color is not None and fill_color is None:
            self.fill_color = plot_color

        self._corrected_energy = None
        self._environment_factors = {}

        self.environment = None
        self.is_reservoir = False

    @property
    def plot_label(self) -> str:
        if isinstance(self._plot_label, Formula):
            return self._plot_label.format()
        else:
            return self._plot_label

    def set_environment(self,
                        environment: List[Species]) -> None:
        if self.is_reservoir:
            raise ValueError('Cannot set environment on solid-phase species in reservoir')

        if not isinstance(environment[0], ReferenceSpecies):
            raise TypeError('The first species in the environment should be reference species')

        all_species = environment + [self]
        nus = balance_equation(all_species)
        energies = np.array([species.energy for species in environment] + [self._uncorrected_energy])

        self._environment_factors = {species: nu
                                     for species, nu in zip(environment, nus)}
        self._corrected_energy = np.dot(nus, energies)

        self.environment = environment

    def set_reservoir(self) -> None:
        if self.environment is not None:
            raise ValueError('Cannot set solid-phase species with environment as reservoir')

        self.is_reservoir = True
        self._corrected_energy = self._uncorrected_energy

    def _needs_environment(func) -> Callable:
        @functools.wraps(func)
        def wrapper(self: SolidPhaseSpecies, *args, **kwargs) -> Any:
            if not self.is_reservoir and self.environment is None:
                raise ValueError('Species needs an environment')
            return func(self, *args, **kwargs)
        return wrapper

    @property
    @_needs_environment
    def energy(self) -> float:
        return self._corrected_energy

    @property
    @_needs_environment
    def environment_factors(self) -> Dict[Species, float]:
        return self._environment_factors

    def get_free_energy_equation(self,
                                 variable_species: Union[GasPhaseSpecies, List[GasPhaseSpecies]],
                                 fixed_mus: Optional[Dict[GasPhaseSpecies, float]] = None) -> LinearEquation:
        if isinstance(variable_species, GasPhaseSpecies):
            variable_species = [variable_species]

        if fixed_mus is None:
            fixed_mus = {}

        fixed_corrections = [mu * species.mu_factor * self.environment_factors.get(species)
                             for species, mu in fixed_mus.items()]
        correction_factors = [species.mu_factor * self.environment_factors.get(species)
                              for species in variable_species]

        return LinearEquation([self.energy + sum(fixed_corrections)] + correction_factors,
                              variable_species)

    def evaluate_free_energy(self,
                             mus: Optional[Dict[GasPhaseSpecies, float]] = None) -> float:
        if mus is None:
            mus = {}

        corrections = [mu * species.mu_factor * self.environment_factors.get(species)
                       for species, mu in mus.items()]
        return self.energy + sum(corrections)

    def to_reference(self) -> ReferenceSpecies:
        return ReferenceSpecies(atoms=self._atoms, energy=self._uncorrected_energy)


class ReferenceSpecies(Species):
    pass


class GasPhaseSpecies(Species):
    def __init__(self,
                 atoms: Atoms,
                 energy: Optional[Union[Callable[[Atoms], float], float]] = None,
                 janaf_name: Optional[str] = None):
        super().__init__(atoms, energy)

        if janaf_name is None:
            janaf_name = self.formula

        self.janaf_name = janaf_name
        self._chemical_potential_calculator = None

        if len(self.formula) == 1:
            self.mu_factor = self.formula.count()
            self.mu_formula = Formula({list(self.formula.keys())[0]: 1})
        else:
            self.mu_factor = 1
            self.mu_formula = self.formula.copy()

    @functools.cached_property
    def chemical_potential_calculator(self) -> IdealGasChemicalPotential:
        return IdealGasChemicalPotential(self.mu_factor, self.janaf_name)

    def dmu(self, T: float, p: float) -> float:
        return self.chemical_potential_calculator.dmu(T, p)

    def dmus(self, conditions: List[Dict[Literal['T', 'p'], float]]) -> List[float]:
        calculator = self.chemical_potential_calculator
        return [calculator.dmu(**cond) for cond in conditions]
