from __future__ import annotations

import itertools
from typing import TYPE_CHECKING, List

import numpy as np
from numpy.typing import NDArray

if TYPE_CHECKING:
    from abinitio.species import Species


def balance_equation(all_species: List[Species],
                     *,
                     fixed_index: int = 0,
                     fixed_value: float = -1.0) -> NDArray:
    symbols = sorted(list(set.union(*(set(species.formula.keys())
                                      for species in all_species))))

    A = np.zeros((len(symbols) + 1, len(all_species)))
    b = np.zeros(len(symbols) + 1)

    for i, j in itertools.product(range(len(symbols)), range(len(all_species))):
        symbol = symbols[i]
        species = all_species[j]

        A[i, j] = species.formula.get(symbol)

    A[-1, fixed_index] = 1
    b[-1] = fixed_value

    nus, _, rank, _ = np.linalg.lstsq(A, b, rcond=None)

    if rank < len(symbols) or not np.isclose(nus[fixed_index], fixed_value):
        equation_str = ' + '.join(str(species.formula) for species in all_species)
        raise ValueError(f'Equation {equation_str} cannot be balanced')

    return nus
