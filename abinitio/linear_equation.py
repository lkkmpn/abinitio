from __future__ import annotations

from typing import TYPE_CHECKING, Dict, Optional, Sequence

import numpy as np

from abinitio.geometry import Line2D, Plane3D

if TYPE_CHECKING:
    from abinitio.species import GasPhaseSpecies


class LinearEquation:
    def __init__(self, coefficients: Sequence[float], variable_species: Sequence[GasPhaseSpecies]):
        if len(coefficients) != len(variable_species) + 1:
            raise ValueError('There must be one more coefficient than variable species')

        self.coefficients = np.array(coefficients)
        self.variable_species = list(variable_species)

    def get_coefficient(self, species: GasPhaseSpecies) -> float:
        return self.coefficients[self._get_coefficient_index(species)]

    def get_intercept(self) -> float:
        return self.coefficients[0]

    def evaluate(self, mus: Dict[GasPhaseSpecies, float]) -> float:
        if set(self.variable_species) != set(mus.keys()):
            raise ValueError('Not all gas-phase species present in mus. '
                             f'Expected: {self.variable_species}, got: {list(mus.keys())}')

        x = np.zeros(self.coefficients.size)
        x[0] = 1  # constant coefficient
        for species, mu in mus.items():
            x[self._get_coefficient_index(species)] = mu

        return np.dot(self.coefficients, x)

    def _get_coefficient_index(self, species: Optional[GasPhaseSpecies]) -> int:
        if species is not None:
            return self.variable_species.index(species) + 1
        else:
            return 0

    def to_line(self, x_species: GasPhaseSpecies) -> Line2D:
        return Line2D(
            self.get_coefficient(x_species),
            -1.0,
            self.get_intercept()
        )

    def to_plane(self, x_species: GasPhaseSpecies, y_species: GasPhaseSpecies) -> Plane3D:
        return Plane3D(
            self.get_coefficient(x_species),
            self.get_coefficient(y_species),
            -1.0,
            self.get_intercept()
        )

    def __repr__(self) -> str:
        return f'LinearEquation(coefficients={self.coefficients})'
