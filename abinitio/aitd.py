from typing import List, Optional

from matplotlib.axes import Axes

from abinitio.plot import AITDPlotter
from abinitio.species import (GasPhaseSpecies, ReferenceSpecies,
                              SolidPhaseSpecies)


class AITD:
    def __init__(self,
                 solid_phase: List[SolidPhaseSpecies],
                 reference: ReferenceSpecies,
                 gas_phase_reservoir: List[GasPhaseSpecies],
                 solid_phase_reservoir: Optional[List[SolidPhaseSpecies]] = None):

        if solid_phase_reservoir is None:
            solid_phase_reservoir = []

        if not all(isinstance(species, SolidPhaseSpecies) for species in solid_phase):
            raise TypeError(f'All elements in solid_phase must be SolidPhaseSpecies')
        if not isinstance(reference, ReferenceSpecies):
            raise TypeError('reference must be ReferenceSpecies')
        if not all(isinstance(species, GasPhaseSpecies) for species in gas_phase_reservoir):
            raise TypeError(f'All elements in gas_phase_reservoir must be GasPhaseSpecies')
        if not all(isinstance(species, SolidPhaseSpecies) for species in solid_phase_reservoir):
            raise TypeError(f'All elements in solid_phase_reservoir must be SolidPhaseSpecies')

        self.solid_phase = solid_phase
        self.reference = reference
        self.gas_phase_reservoir = gas_phase_reservoir
        self.solid_phase_reservoir = solid_phase_reservoir

        for species in self.solid_phase_reservoir:
            species.set_reservoir()

        for species in self.solid_phase:
            species.set_environment([reference] + gas_phase_reservoir + solid_phase_reservoir)

    def get_plotter(self, ax: Axes) -> AITDPlotter:
        return AITDPlotter(self, ax)
