from __future__ import annotations

import json
import os
from enum import IntEnum
from pathlib import Path
from typing import List, Tuple, Union

import numpy as np
import requests

from abinitio.formula import Formula

CACHE_ROOT = Path.home() / '.cache' / 'janaf'
REMOTE_INDEX_URL = 'https://janaf.nist.gov/dat/janaf.json'
REMOTE_ENTRY_URL = 'https://janaf.nist.gov/tables/{code}.txt'


def get_janaf_by_name(name: Union[str, Formula]) -> JANAFEntry:
    names, codes = _get_index()

    if isinstance(name, Formula):
        def match_fn(formula: Formula, other: str) -> bool:
            other_formula_str = other.split(', ')[0]
            if '+' in other_formula_str or '-' in other_formula_str:
                # no support for ions
                return False

            try:
                other_formula = Formula.from_str(other_formula_str)
            except ValueError:
                return False

            return formula == other_formula
    else:
        def match_fn(name: str, other: str) -> bool:
            return name in other

    indices: List[int] = []
    for index, other in enumerate(names):
        if match_fn(name, other):
            indices.append(index)

    if len(indices) == 0:
        raise ValueError(f'{name} was not found in the JANAF database.')
    if len(indices) > 1:
        raise ValueError(f'Multiple possibilities found in the JANAF database for {name}: ' +
                         (", ".join(f'"{names[index]}"' for index in indices)))

    index = indices[0]
    return get_janaf_by_code(codes[index])


def get_janaf_by_code(code: str) -> JANAFEntry:
    data_path = _get_file_cache(f'{code}.txt', REMOTE_ENTRY_URL.format(code=code))
    return JANAFEntry(data_path)


def _get_index() -> Tuple[List[str], List[str]]:
    index_path = _get_file_cache('janaf.json', REMOTE_INDEX_URL)
    with open(index_path, 'r') as f:
        index = json.load(f)

    names = index['display']
    codes = index['index']

    return names, codes


def _get_file_cache(path: os.PathLike,
                    remote_url: str,
                    force_reload: bool = False,
                    **request_kwargs) -> Path:
    CACHE_ROOT.mkdir(parents=True, exist_ok=True)
    cache_path = CACHE_ROOT / path

    if not force_reload and cache_path.exists():
        return cache_path

    r = requests.get(remote_url, stream=True, **request_kwargs)
    r.raise_for_status()

    with open(cache_path, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            f.write(chunk)

    return cache_path


def _converter(val) -> float:
    try:
        return float(val)
    except:
        if val == b'INFINITE':
            return np.inf
        else:
            return np.nan


class JANAFIndex(IntEnum):
    T = 0
    Cp = 1
    S = 2
    Gef = 3
    Hef = 4
    dHf = 5
    dGf = 6
    logKf = 7


class JANAFEntry:
    def __init__(self, data_path: Path):
        with open(data_path, 'r') as f:
            self.header_line = f.readline().strip()

            # note `skip_header=1` as we have already consumed one header line
            self.data = np.genfromtxt(f,
                                      skip_header=1,
                                      converters={i: _converter for i in range(8)},
                                      usecols=range(8))

        self.Tr = 298.15  # K
        self.p0 = 1e5  # Pa

        self.T_unit = self.Tr_unit = 'K'
        self.Cp_unit = self.S_unit = self.Gef_unit = 'J/K/mol'
        self.Hef_unit = self.dHf_unit = self.dGf_unit = 'kJ/mol'
        self.logKf_unit = ''

        self.T_data = self.data[:, JANAFIndex.T]
        self.Cp_data = self.data[:, JANAFIndex.Cp]
        self.S_data = self.data[:, JANAFIndex.S]
        self.Gef_data = self.data[:, JANAFIndex.Gef]
        self.Hef_data = self.data[:, JANAFIndex.Hef]
        self.dHf_data = self.data[:, JANAFIndex.dHf]
        self.dGf_data = self.data[:, JANAFIndex.dGf]
        self.logKf_data = self.data[:, JANAFIndex.logKf]

    def _interp(self, x: float, x_index: JANAFIndex, f_index: JANAFIndex) -> float:
        return np.interp(x, self.data[:, x_index], self.data[:, f_index])

    def T_range(self) -> Tuple[float, float]:
        return np.min(self.data[:, JANAFIndex.T]), np.max(self.data[:, JANAFIndex.T])

    def Cp(self, T: float) -> float:
        return self._interp(T, JANAFIndex.T, JANAFIndex.Cp)

    def S(self, T: float) -> float:
        return self._interp(T, JANAFIndex.T, JANAFIndex.S)

    def Gef(self, T: float) -> float:
        return self._interp(T, JANAFIndex.T, JANAFIndex.Gef)

    def Hef(self, T: float) -> float:
        return self._interp(T, JANAFIndex.T, JANAFIndex.Hef)

    def dHf(self, T: float) -> float:
        return self._interp(T, JANAFIndex.T, JANAFIndex.dHf)

    def dGf(self, T: float) -> float:
        return self._interp(T, JANAFIndex.T, JANAFIndex.dGf)

    def logKf(self, T: float) -> float:
        return self._interp(T, JANAFIndex.T, JANAFIndex.logKf)

    def __repr__(self) -> str:
        return f'<JANAFEntry {self.header_line}>'
