from typing import Callable, Union

import numpy as np
from ase import units
from numpy.typing import NDArray

from abinitio.formula import Formula
from abinitio.janaf import get_janaf_by_name


class IdealGasChemicalPotential:
    def __init__(self, mu_factor: float, janaf_name: Union[str, Formula]):
        self.mu_factor = mu_factor

        janaf_entry = get_janaf_by_name(janaf_name)

        T0 = np.flatnonzero(janaf_entry.T_data == 0)[0]

        self.T_data = janaf_entry.T_data

        dH_data = (janaf_entry.Hef_data - janaf_entry.Hef_data[T0]) * units.kJ / units.mol
        dS_data = (janaf_entry.S_data - janaf_entry.S_data[T0]) * units.J / units.mol
        self.dmu0_data = dH_data - self.T_data * dS_data

        assert (np.all(np.diff(self.dmu0_data) < 0) or
                np.all(np.diff(self.dmu0_data) > 0))

    def dmu(self, T: float, p: float) -> float:
        return (np.interp(T, self.T_data, self.dmu0_data) +
                units.kB * T * np.log(p)) / self.mu_factor

    def _log10p_to_dmu(self, T: float) -> Callable[[NDArray], NDArray]:
        def func(log10p: NDArray) -> NDArray:
            return (np.interp(T, self.T_data, self.dmu0_data) +
                    units.kB * T * np.log(10) * log10p) / self.mu_factor
        return func

    def _dmu_to_log10p(self, T: float) -> Callable[[NDArray], NDArray]:
        def func(dmu: NDArray) -> NDArray:
            return 1 / units.kB / T / np.log(10) * \
                (self.mu_factor * dmu - np.interp(T, self.T_data, self.dmu0_data))
        return func

    def _T_to_dmu(self, log10p: float) -> Callable[[NDArray], NDArray]:
        def func(T: NDArray) -> NDArray:
            return (np.interp(T, self.T_data, self.dmu0_data) +
                    units.kB * T * np.log(10) * log10p) / self.mu_factor
        return func

    def _dmu_to_T(self, log10p: float) -> Callable[[NDArray], NDArray]:
        def func(dmu: NDArray) -> NDArray:
            dmu0_p_data = self.dmu0_data + \
                units.kB * self.T_data * np.log(10) * log10p
            assert (np.all(np.diff(dmu0_p_data) < 0) or
                    np.all(np.diff(dmu0_p_data) > 0))
            sign = int(np.sign(np.diff(dmu0_p_data)[0]))
            return np.interp(dmu, dmu0_p_data[::sign] / self.mu_factor, self.T_data[::sign])
        return func
