import numpy as np


def approx_lt(a, b, rtol=1e-5, atol=1e-8) -> bool:
    """Elementwise approximately less than operator. Returns True if `a` is
    less than `b` and the values are not close to each other.
    """
    return (a < b) & ~np.isclose(a, b, rtol=rtol, atol=atol)


def approx_le(a, b, rtol=1e-5, atol=1e-8) -> bool:
    """Elementwise approximately less than or equal to operator. Returns True
    if `a` is less than or equal to `b` or the values are close to each other.
    """
    return (a <= b) | np.isclose(a, b, rtol=rtol, atol=atol)


def approx_gt(a, b, rtol=1e-5, atol=1e-8) -> bool:
    """Elementwise approximately greater than operator. Returns True if `a` is
    greater than `b` and the values are not close to each other.
    """
    return (a > b) & ~np.isclose(a, b, rtol=rtol, atol=atol)


def approx_ge(a, b, rtol=1e-5, atol=1e-8) -> bool:
    """Elementwise approximately greater than or equal to operator. Returns
    True if `a` is greater than or equal to `b` or the values are close to each
    other.
    """
    return (a >= b) | np.isclose(a, b, rtol=rtol, atol=atol)
