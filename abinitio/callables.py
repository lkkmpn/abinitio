from typing import Sequence, Union

from ase.atoms import Atoms
from ase.formula import Formula as AseFormula


def ase_formula_without_symbols(atoms: Atoms, symbols: Union[str, Sequence[str]]) -> AseFormula:
    if isinstance(symbols, str):
        symbols = [symbols]

    count = atoms.symbols.formula.count()
    for symbol in symbols:
        count[symbol] = 0

    return AseFormula.from_dict(count)
