from __future__ import annotations

import functools
from dataclasses import dataclass
from typing import Optional, Tuple

import numpy as np
from numpy.typing import NDArray


@dataclass(frozen=True)
class Line2D:
    """A line in two dimensions represented by the general equation
    ax + by + c = 0.
    """
    a: float
    b: float
    c: float

    def evaluate_y(self, x: float) -> float:
        """Evaluate the y coordinate of the line at a given x coordinate.

        Parameters
        ----------
        x : float
            x coordinate to evaluate the line at.

        Returns
        -------
        float
            y coordinate at the given x coordinate.
        """
        return -(self.c + self.a * x) / self.b

    @functools.cached_property
    def _nl(self) -> float:
        """The normal vector to the line and its length. The normal vector to
        the line is given as n = (a, b).
        """
        n = np.array([self.a, self.b])
        return n, np.linalg.norm(n)

    @functools.cached_property
    def n(self) -> NDArray:
        """The unit normal vector to the line, used in its Hessian normal form:
        n · x = -p.
        """
        n, l = self._nl
        return n / l

    @functools.cached_property
    def p(self) -> float:
        """The signed distance from the line to the origin, used in its Hessian
        normal form: n · x = -p.

        If p > 0, the origin is located in the half-space determined by the
        direction of n; if p < 0, the origin is located in the other
        half-space.
        """
        _, l = self._nl
        return self.c / l

    @staticmethod
    def two_line_intersection(*ls: Line2D) -> Optional[NDArray]:
        """Get the unique point of intersection of two lines. If the lines are
        parallel, there is no unique point of intersection and the function
        returns None.

        Returns
        -------
        Optional[NDArray]
            Unique point of intersection, or None if it does not exist.
        """
        if len(ls) != 2:
            raise ValueError(f'Two lines required; {len(ls)} provided.')

        if not all(isinstance(l, Line2D) for l in ls):
            raise TypeError(f'Not all input arguments are `Line2D` objects.')

        # https://en.wikipedia.org/wiki/Intersection_(geometry)#Two_lines
        M = np.array([[l.a, l.b] for l in ls])
        det = np.linalg.det(M)

        if np.isclose(det, 0):
            # lines are parallel; there is no intersection
            return None

        # point of intersection
        x = (ls[1].c * ls[0].b - ls[0].c * ls[1].b) / det
        y = (ls[0].c * ls[1].a - ls[1].c * ls[0].a) / det

        return np.array([x, y])


@dataclass(frozen=True)
class Plane3D:
    """A plane in three dimensions represented by the general equation
    ax + by + cz + d = 0.
    """
    a: float
    b: float
    c: float
    d: float

    def evaluate_z(self, x: float, y: float) -> float:
        """Evaluate the z coordinate of the plane at given x and y coordinates.

        Parameters
        ----------
        x : float
            x coordinate to evaluate the plane at.
        y : float
            y coordinate to evaluate the plane at.

        Returns
        -------
        float
            z coordinate at the given x and y coordinates.
        """
        return -(self.d + self.a * x + self.b * y) / self.c

    @functools.cached_property
    def _nl(self) -> Tuple[NDArray, float]:
        """The normal vector to the plane and its length. The normal vector to
        the plane is given as n = (a, b, c).
        """
        n = np.array([self.a, self.b, self.c])
        return n, np.linalg.norm(n)

    @functools.cached_property
    def n(self) -> NDArray:
        """The unit normal vector to the plane, used in its Hessian normal
        form: n · x = -p.
        """
        n, l = self._nl
        return n / l

    @functools.cached_property
    def p(self) -> float:
        """The signed distance from the plane to the origin, used in its
        Hessian normal form: n · x = -p.

        If p > 0, the origin is located in the half-space determined by the
        direction of n; if p < 0, the origin is located in the other
        half-space.
        """
        _, l = self._nl
        return self.d / l

    @staticmethod
    def three_plane_intersection(*ps: Plane3D) -> Optional[NDArray]:
        """Get the unique point of intersection of three planes. If at least
        two of the planes are parallel, there is no unique point of
        intersection and the function returns None.

        Returns
        -------
        Optional[NDArray]
            Unique point of intersection, or None if it does not exist.
        """
        if len(ps) != 3:
            raise ValueError(f'Three planes required; {len(ps)} provided.')

        if not all(isinstance(p, Plane3D) for p in ps):
            raise TypeError('Not all input arguments are `Plane3D` objects.')

        # https://mathworld.wolfram.com/Plane-PlaneIntersection.html
        N = np.vstack([p.n for p in ps])
        det = np.linalg.det(N)

        if np.isclose(det, 0):
            # at least two planes are parallel; there is no intersection
            return None

        # arbitrary points on each plane
        xs = [-p.n * p.p for p in ps]

        # point of intersection
        x = ((np.dot(xs[0], ps[0].n) * np.cross(ps[1].n, ps[2].n) +
              np.dot(xs[1], ps[1].n) * np.cross(ps[2].n, ps[0].n) +
              np.dot(xs[2], ps[2].n) * np.cross(ps[0].n, ps[1].n))
             / det)

        return x
